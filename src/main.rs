use clap::Parser;
use libjeremy::Table;
use std::{fs::OpenOptions, io::Read, path::PathBuf};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(index = 1)]
    file_name: PathBuf,
}

fn main() {
    let args = Args::parse();

    let mut file = OpenOptions::new().read(true).open(args.file_name).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();

    let table = Table::try_from(contents.as_str()).unwrap();
    print!("{:?}", table);
}
