//! Phase 1: Parsing
//! Phase 2: Evaluating
//! Phase 3: Displaying
use nom::{
    bytes::complete::{tag, take_till, take_while1},
    combinator::{map, map_res},
    error::{Error as NomError, VerboseError},
    multi::separated_list1,
    ErrorConvert, Finish,
};
use std::{
    collections::HashSet,
    fmt::Display,
    ops::{Index, IndexMut},
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum JeremyError {
    #[error("failed to parse cell")]
    FailedCell(String),
    #[error("failed to parse table")]
    FailedTable,
}

// impl From<NomError<&str>> for std::result::Result<Ident, JeremyError> {
//     fn from(value: NomError<&str>) -> Self {
//         Err(value.into())
//     }
// }
//

// impl ErrorConvert<JeremyError> for NomError<&str> {
//     fn convert(self) -> JeremyError {
//         JeremyError::FailedCell(self.input.to_string())
//     }
// }
//
// impl From<(&str, nom::error::ErrorKind)> for JeremyError {
//     fn from(value: (&str, nom::error::ErrorKind)) -> Self {
//         Self::FailedCell(value.0.to_string())
//     }
// }

impl<T: ToString> From<NomError<T>> for JeremyError {
    fn from(value: NomError<T>) -> Self {
        Self::FailedCell(value.input.to_string())
    }
}

#[derive(Debug, Clone)]
enum SymbolTY {
    Add,
    Sub,
    Mul,
    Div,
}

#[derive(Debug, Clone)]
enum FunctionToken {
    Value(Ident),
    Symbol(SymbolTY),
    Min(Box<FunctionTokens>),
    Max(Box<FunctionTokens>),
    SQRT(Box<FunctionTokens>),
    FN(Box<FunctionTokens>),
}

#[derive(Debug, Clone)]
struct FunctionTokens(Ident, Vec<FunctionToken>);

impl FunctionTokens {
    fn evaluate(&self) -> Result<Cell, JeremyError> {
        let mut cycle: HashSet<Ident> = HashSet::new();
        let mut value = 0;
        for token in &self.1 {}

        todo!()
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Cell {
    Empty,
    // Function(FunctionTY, Vec<Ident>),
    // =PLUS(A1; A5, A6)
    // =A1 + A5 - A6 * B5
    // =SQRT(A3)
    // =MAX()
    // =MIN()
    //
    // Function(FunctionTokens),
    Function,
    Number(i128),
    String(String),
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Ident {
    row: usize,
    col: usize,
}

impl Index<Ident> for Table {
    type Output = Cell;

    fn index(&self, index: Ident) -> &Self::Output {
        let index = index.col * self.row_length + index.row;
        self.cells.get(index).unwrap()
    }
}

impl IndexMut<Ident> for Table {
    fn index_mut(&mut self, index: Ident) -> &mut Self::Output {
        let index = index.col * self.row_length + index.row;
        self.cells.get_mut(index).unwrap()
    }
}

impl<'a> TryFrom<&'a str> for Ident {
    type Error = JeremyError;

    fn try_from(input: &'a str) -> Result<Self, Self::Error> {
        let (input, alpha) =
            take_while1::<_, _, NomError<&str>>(|c: char| c.is_alphabetic())(input).finish()?;
        let (_input, col) = map_res::<_, &str, _, NomError<&str>, _, _, _>(
            take_while1(|c: char| c.is_numeric()),
            |s: &str| s.parse::<usize>(),
        )(input)
        .finish()?;
        let alpha = alpha.to_uppercase();
        assert!(col > 0, "indexing into table with 0 is not allowed");

        let row = alpha.chars().fold(0, |acc, c| (acc * 26) + c as usize - 65);

        Ok(Ident { col: col - 1, row })
    }
}

#[derive(Debug)]
pub struct Table {
    // Flat List: List contains cells
    // Structured List: List of Lists that contain Cell
    cells: Vec<Cell>,
    row_length: usize,
    col_length: usize,
}

impl<'a> From<&'a str> for Cell {
    fn from(input: &str) -> Self {
        match input.parse::<i128>() {
            Ok(v) => Cell::Number(v),
            _ if input.is_empty() => Cell::Empty,
            _ if input.starts_with('=') => Cell::Function,
            _ => Cell::String(input.to_owned()),
        }
    }
}

impl<'a> TryFrom<&'a str> for Table {
    type Error = JeremyError;

    fn try_from(input: &'a str) -> Result<Self, Self::Error> {
        let lines = input.lines();
        let mut row_length = 0;
        // max_cells_for_rows
        let col_length = lines.clone().count();
        for line in lines {
            let rows = line.matches(',').count();
            if rows > row_length {
                row_length = rows;
            }
        }

        let mut cells = vec![Cell::Empty; row_length * col_length];
        for (current_col, input) in input.lines().enumerate() {
            let (_input, value) = separated_list1::<_, _, _, NomError<&str>, _, _>(
                tag(","),
                map(take_till(|c| c == ','), Cell::from),
            )(input)
            .finish()?;
            for (index, value) in value.into_iter().enumerate() {
                cells[(row_length * current_col) + index] = value.to_owned();
            }
        }

        Ok(Self {
            cells,
            row_length,
            col_length,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::{Cell, Ident, Table};

    static FILE: &str = include_str!("../test_files/test.csv");
    #[test]
    fn test1() {
        let k = Table::try_from(FILE).unwrap();
        assert_eq!(
            k.cells,
            [
                Cell::Number(1),
                Cell::Number(3),
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Number(5),
                Cell::Number(5),
                Cell::Empty,
                Cell::Number(6),
                Cell::Number(7),
                Cell::Empty,
                Cell::Number(65),
                Cell::Number(6),
                Cell::Empty,
                Cell::Number(7),
                Cell::Empty,
                Cell::Number(7),
                Cell::Number(8),
                Cell::Number(8),
                Cell::Empty,
                Cell::Empty,
                Cell::Number(8),
                Cell::Number(78),
                Cell::Empty,
                Cell::Empty,
                Cell::Number(5),
                Cell::Number(345),
                Cell::Number(4),
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Function,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Number(1),
                Cell::String("sdsd".to_owned()),
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Number(2),
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
                Cell::Empty,
            ]
        );
    }

    #[test]
    fn test2() {
        let k = Table::try_from(FILE).unwrap();
        dbg!(k.cells.len());
        let ident = dbg!(Ident::try_from("B1").unwrap());
        assert_eq!(Ident::try_from("A40").unwrap(), Ident { row: 0, col: 39 });
        assert_eq!(Ident::try_from("B2").unwrap(), Ident { row: 1, col: 1 });
        assert_eq!(k[dbg!(ident)], Cell::Number(3));
        assert_eq!(k[Ident::try_from("B2").unwrap()], Cell::Number(65));
    }
}
